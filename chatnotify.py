#! /usr/bin/env python
# Send a notification using Google Chat

import argparse
import logging
import os
import time
import json
import requests

def parse_cli_args():
    """ Command line argument processing """
    help = {'verbose': 'More verbose output',
            'message': 'The message to send',
            'webhook': 'The webhook to use to post the message, this or token is required',
            }

    parser = argparse.ArgumentParser(prog='chatnotify',
                                     description='Utility to send Google Chat notification from the commandline')

    parser.add_argument('-v', '--verbose', action='store_true', default=False,      help=help['verbose'])
    parser.add_argument('-m', '--message', action='store',      required=True,      help=help['message'])
    parser.add_argument('-w', '--webhook', action='store',      required=True,      help=help['webhook'])

    args = parser.parse_args()
    return args


def sendmsg(config):

    chat_data = {'text': config['message'] }
    response = requests.post(
        config['webhook'], data=json.dumps(chat_data),
        headers={'Content-Type': 'application/json'}
    )
    if response.status_code != 200:
        raise ValueError(
            'Request returned error %s, the response is:\n%s'
            % (response.status_code, response.text)
        )

    config['logger'].info('Message sent: %s' % config['message'])


def setup_logger():
    logger = logging.getLogger('chatnotify')
    logger.setLevel(logging.INFO)

    fh = logging.FileHandler('/var/log/chatnotify.log')
    ch = logging.StreamHandler()

    formatter_file = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter_stderr = logging.Formatter('%(name)s - %(levelname)s - %(message)s')

    fh.setFormatter(formatter_file)
    fh.setLevel(logging.INFO)

    ch.setFormatter(formatter_stderr)
    ch.setLevel(logging.ERROR)

    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger


def message_from_checkmk_env(logger):
    host = os.environ.get('NOTIFY_HOSTNAME', 'Unknown host')
    host_output = os.environ.get('NOTIFY_HOSTOUTPUT', '')
    host_state = os.environ.get('NOTIFY_HOSTSTATE', '')
    serv_output = os.environ.get('NOTIFY_SERVICEOUTPUT', '')
    serv_state = os.environ.get('NOTIFY_SERVICESTATE', '')
    serv_descr = os.environ.get('NOTIFY_SERVICEDESC', '')
    what = os.environ.get('NOTIFY_WHAT', '')
    site = os.environ.get('NOTIFY_OMD_SITE', '').lower()

    logger.debug('NOTIFY_OMD_SITE: %s' % site)
    logger.debug('NOTIFY_WHAT: %s' % what)
    logger.debug('NOTIFY_HOSTNAME: %s' % host)
    logger.debug('NOTIFY_HOSTOUTPUT: %s' % host_output)
    logger.debug('NOTIFY_HOSTSTATE: %s' % host_state)
    logger.debug('NOTIFY_SERVICEOUTPUT: %s' % serv_output)
    logger.debug('NOTIFY_SERVICESTATE: %s' % serv_state)
    logger.debug('NOTIFY_NOTIFY_SERVICEDESC: %s' % serv_descr)

    message = ''
    if what == 'HOST':
        message = "%s: %s - %s" % (host, host_state, host_output)
    if what == 'SERVICE':
        message = "%s: %s - %s - %s" % (host, serv_descr, serv_state, serv_output)

    return message


def main():
    logger = setup_logger()
    logger.info('Starting chatnotify')
    config = {'logger': logger, 'channel': None, 'token': None, 'message': None}

    if os.environ.get('NOTIFY_DATE'):
        # Called from a CheckMK notification context. This requires:
        #   - NOTIFY_PARAMETER_1 to contain the chat token
        #   - channel is #<omd instance name>, unless NOTIFY_PARAMETER_2 is defined
        webhook = None

        try:
            webhook = os.environ['NOTIFY_PARAMETER_1']
        except:
            logger.info('NOTIFY_PARAMETER_1 (webhook) not set, and is required')
            raise

        try:
            channel = os.environ['NOTIFY_PARAMETER_2']
        except:
            logger.info(
                'NOTIFY_PARAMETER_2 (chat channel) not set, trying to use NOTIFY_OMD_SITE to determine channel')
            try:
                channel = '#' + os.environ['NOTIFY_OMD_SITE'].lower()
            except:
                logger.error('NOTIFY_OMD_SITE not defined')
            else:
                logger.info('Getting all information from check_mk NOTIFY_ environment variables')
                config = {'verbose': False,
                          'message': message_from_checkmk_env(logger),
                          'logger': logger,
                          'webhook': webhook,
                         }

    else:
        args = parse_cli_args()
        logger.info('Getting all information from the commandline')
        config = {
            'verbose': args.verbose,
            'message': args.message,
            'now': time.strftime('%Y%m%d%H%M'),
            'logger': logger,
            'webhook': args.webhook,
        }

    logger.info('webhook: %s' % config['webhook'])
    logger.info('message: %s' % config['message'])

    sendmsg(config)
    logger.info('Ending chatnotify')

main()
